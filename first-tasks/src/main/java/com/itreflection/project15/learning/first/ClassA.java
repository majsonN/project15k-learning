package com.itreflection.project15.learning.first;

public class ClassA {

  private boolean isTrue;

  public ClassA() {
    this.isTrue = true;
  }

  public boolean isTrue() {
    return isTrue;
  }
  public void setTrue(boolean aTrue) {
    isTrue = aTrue;
  }
}
