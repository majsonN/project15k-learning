package com.itreflection.project15.learning.task1.impl;

import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class TriangleCalculationServiceImplTest {

  private final Logger logger = getLogger(TriangleCalculationServiceImplTest.class);
//123
  @InjectMocks
  private TriangleCalculationServiceImpl triangleCalculationService;

  @Test
  public void test()
  {
    TriangleCalculationServiceImpl t1 = new TriangleCalculationServiceImpl();
    double output = t1.calculatePerimeter(1,5,2);
    if(output<0) {
      output *= (-1);                           // obwod nie moze byc ujemn
      Assert.assertTrue((output*(-1) == t1.x + t1.y + t1.z));
    }
    else
      Assert.assertTrue((output == t1.x + t1.y + t1.z));

  }

}